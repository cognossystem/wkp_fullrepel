jQuery(document).ready(function(){
    
	jQuery('header .mobile').click(function(){
		if(jQuery('menu').hasClass('active')){
			jQuery('menu').removeClass('active');
		}else{
			jQuery('menu').addClass('active');
		}
	});

	jQuery('header .container menu nav li a').click(function(e){
		jQuery('header .container menu nav li a').removeClass('active');
		jQuery(this).addClass('active');

		id = jQuery(this).attr('href');
		jQuery('html, body').animate({
            scrollTop: (jQuery(id).offset().top -140)
        }, 500,'linear');


		e.preventDefault();
	});

	jQuery('form').submit(function(e){

		jQuery.post( "form.php", jQuery(this).serialize()).done(function( data ) {
    		alert( "E-mail enviado com sucesso");
  		});
		e.preventDefault();
	});
});